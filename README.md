Pediatric Care is a smaller, private pediatric practice whose size allows for more attention to the children. They have served families in Utah County for over 25 years and are one of the few offices accepting Medicaid in the Valley.

Address: 1675 North Freedom Blvd, #3B, Provo, UT 84604, USA

Phone: 801-377-8000